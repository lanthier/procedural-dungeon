﻿namespace Scripts.Dungeon
{
	using System;
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;
	using Random = UnityEngine.Random;
	using Models;
	using Util;

	public class DungeonGenerator : MonoBehaviour {

		[Serializable]
		public class Count 
		{
			public int minimum;
			public int maximum;

			public Count (int min, int max)
			{
				minimum = min;
				maximum = max;
			}
		}

		public int columns = 64;
		public int rows = 64;

		public GameObject[] floorTiles;

		private Transform boardHolder;
		private List <Vector3> gridPositions = new List<Vector3> ();

		void InitializeList()
		{
			gridPositions.Clear();

			for(int x = 0; x < columns; x++) 
			{
				for(int y = 0; y < columns; y++)
				{
					gridPositions.Add (new Vector3(x, y, 0f));
				}
			}
		}

		private void Generate(DungeonOptions dungeonOptions) 
		{
			boardHolder = new GameObject ("Board").transform;
			GenerateRooms (dungeonOptions.RoomCount);

		}

		public void GenerateDungeon(DungeonOptions dungeonOptions)
		{
			Generate (dungeonOptions);
			InitializeList ();

		}

		private void GenerateRooms(Variant roomCount) 
		{
			int sectionCount = 4;
			//Split up dungeon into sectors
			List<Tuple<Vector3, Vector3>> sectionList = SplitGridIntoSections(sectionCount, new Vector3(0, 0), new Vector3(rows, columns));

			AddRoomsToRandomSections (sectionList, roomCount.Value);
		}

		private List<Tuple<Vector3, Vector3>> SplitGridIntoSections(int sectionCount, Vector3 northWest, Vector3 southEast) 
		{
			List<Tuple<Vector3, Vector3>> sectionList = new List<Tuple<Vector3, Vector3>>();
			var middle = new Vector3 (southEast.x / 2, southEast.y / 2);
			var northMiddle = new Vector3 (southEast.x / 2, 0);
			var eastMiddle = new Vector3 (southEast.x, southEast.y / 2);
			var southMiddle = new Vector3 (southEast.x / 2, southEast.y);
			var westMiddle = new Vector3 (0, southEast.y / 2);

			if(sectionCount == 4)
			{
				sectionList.Add (Tuple.New (northWest, middle));
				sectionList.Add (Tuple.New (northMiddle, eastMiddle));
				sectionList.Add (Tuple.New (westMiddle, southMiddle));
				sectionList.Add (Tuple.New (middle, southEast));
			}
			else 
			{

				var northwestSection = SplitGridIntoSections(sectionCount/4, northWest, middle);
				sectionList.AddRange (northwestSection);

				var northeastSection = SplitGridIntoSections(sectionCount/4, northMiddle, eastMiddle);
				sectionList.AddRange (northeastSection);

				var southwestSection = SplitGridIntoSections(sectionCount/4, westMiddle, southMiddle); 
				sectionList.AddRange (southwestSection);

				var southeastSection = SplitGridIntoSections(sectionCount/4, middle, southEast); 
				sectionList.AddRange (southeastSection);

			}

			return sectionList;
		}

		private void AddRoomsToRandomSections(List<Tuple<Vector3, Vector3>> sectionList, int roomCount)
		{
			if(roomCount == 0)
			{
				return;
			}

			int random = Random.Range (0, sectionList.Count-1);
			var selectedSection = sectionList [random];
			sectionList.RemoveAt (random);

			AddRoom (selectedSection, GenerateDungeonRoomOptions());
			AddRoomsToRandomSections (sectionList, --roomCount);
		}

		private void AddRoom(Tuple<Vector3, Vector3> section, DungeonRoomOptions roomOptions)
		{
			var northWest = section.First;
			var southEast = section.Second;

			var randomX = Random.Range (northWest.x, southEast.x - roomOptions.RoomWidth.Value);
			var randomY = Random.Range (northWest.y, southEast.y - roomOptions.RoomHeight.Value);

			for(var x = randomX; x < randomX + roomOptions.RoomWidth.Value; x++)
			{
				for(var y = randomY; y < randomY + roomOptions.RoomHeight.Value; y++) 
				{
					BuildFloorTile (x, y);
				}
			}
		}

		// Update is called once per frame
		void Update () 
		{
			
		}

		private DungeonRoomOptions GenerateDungeonRoomOptions()
		{
			return new DungeonRoomOptions () {
				RoomWidth = new Variant(4, Variance.Low),
				RoomHeight = new Variant(4, Variance.Low)
			};
		}

		private void BuildFloorTile(float x, float y)
		{
			GameObject toInstantiate = floorTiles [Random.Range (0, floorTiles.Length-1)];
			GameObject instance = Instantiate(toInstantiate, new Vector3(x, y, 0f), Quaternion.identity) as GameObject;
			instance.transform.SetParent (boardHolder);
		}
	}
}

