﻿using System;
using Random = UnityEngine.Random;

namespace Scripts.Dungeon.Models
{
	public class DungeonRoomOptions
	{
		public Variant RoomHeight { get; set; }

		public Variant RoomWidth { get; set; }
	}
}

